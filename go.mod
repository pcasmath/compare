module bitbucket.org/pcasmath/compare

go 1.16

require (
	bitbucket.org/pcasmath/object v0.0.4
	bitbucket.org/pcasmath/slice v0.0.4
)
