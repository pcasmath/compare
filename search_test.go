// Tests of search.go.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package compare

import (
	"bitbucket.org/pcasmath/object"
	"bitbucket.org/pcasmath/slice"
	"errors"
	"strconv"
	"testing"
)

// dummyParent is a dummy parent used for testing.
type dummyParent struct{}

// dummyElement is a dummy element used for testing.
type dummyElement int

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// brokenCmp returns an error on use.
func brokenCmp(_ object.Element, _ object.Element) (int, error) {
	return 0, errors.New("always returns an error")
}

// cmp is a valid comparison function for dummyElements.
func cmp(x object.Element, y object.Element) (int, error) {
	R := dummyParent{}
	t, err := R.ToElement(x)
	if err != nil {
		return 0, err
	}
	xx := t.(dummyElement)
	t, err = R.ToElement(y)
	if err != nil {
		return 0, err
	}
	yy := t.(dummyElement)
	if xx < yy {
		return -1, nil
	} else if xx > yy {
		return 1, nil
	}
	return 0, nil
}

/////////////////////////////////////////////////////////////////////////
// dummyParent functions
/////////////////////////////////////////////////////////////////////////

// String returns a string representation of the parent.
func (dummyParent) String() string {
	return "zZ(int)"
}

// Contains returns true iff x is an element of this parent, or can naturally be regarded as an element of this parent.
func (dummyParent) Contains(x object.Element) bool {
	_, ok := x.(dummyElement)
	return ok
}

// ToElement returns x as an element of this parent, or an error if x cannot naturally be regarded as an element of this parent.
func (dummyParent) ToElement(x object.Element) (object.Element, error) {
	y, ok := x.(dummyElement)
	if !ok {
		return nil, errors.New("argument is not a dummyElement")
	}
	return y, nil
}

// AreEqual returns true iff x and y are both contained in the parent, and x = y.
func (dummyParent) AreEqual(x object.Element, y object.Element) (bool, error) {
	xx, ok := x.(dummyElement)
	if !ok {
		return false, errors.New("argument 1 is not a dummyElement")
	}
	yy, ok := y.(dummyElement)
	if !ok {
		return false, errors.New("argument 2 is not a dummyElement")
	}
	return xx == yy, nil
}

/////////////////////////////////////////////////////////////////////////
// dummyElement functions
/////////////////////////////////////////////////////////////////////////

// String returns a string representation of the element.
func (x dummyElement) String() string {
	return strconv.Itoa(int(x))
}

// Hash returns a hash value for the element.
func (x dummyElement) Hash() uint32 {
	return uint32(x)
}

// Parent returns the parent of the element.
func (dummyElement) Parent() object.Parent {
	return dummyParent{}
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// TestSearchAndIndex tests Search and Index.
func TestSearchAndIndex(t *testing.T) {
	// Create a slice of values
	S := make([]object.Element, 0, 10)
	for i := 0; i < cap(S); i++ {
		S = append(S, dummyElement(2*i+3))
	}
	// Check that we can recover correct indices for elements in this slice
	for i, e := range S {
		idx, err := Search(cmp, slice.ElementSlice(S), e)
		if err != nil {
			t.Fatalf("error recovering element %s: %v", e, err)
		} else if idx != i {
			t.Fatalf("expected index %d for element %s but got index %d", i, e, idx)
		}
		idx, err = Index(cmp, slice.ElementSlice(S), e)
		if err != nil {
			t.Fatalf("error recovering element %s: %v", e, err)
		} else if idx != i {
			t.Fatalf("expected index %d for element %s but got index %d", i, e, idx)
		}
	}
	// Searching for a value larger than those in S returns the length...
	e := dummyElement(3*len(S) + 10)
	idx, err := Search(cmp, slice.ElementSlice(S), e)
	if err != nil {
		t.Fatalf("error performing Search for %s: %v", e, err)
	} else if idx != len(S) {
		t.Fatalf("expected index %d for Search but got index %d", len(S), idx)
	}
	// ...and that the index is -1
	idx, err = Index(cmp, slice.ElementSlice(S), e)
	if err != nil {
		t.Fatalf("error performing Index for %s: %v", e, err)
	} else if idx != -1 {
		t.Fatalf("expected index -1 for Index but got index %d", idx)
	}
	// Let's use a broken comparator that throws an error on use
	_, err = Search(brokenCmp, slice.ElementSlice(S), e)
	if err == nil {
		t.Fatal("expected error on Search with broken comparator")
	}
	_, err = Index(brokenCmp, slice.ElementSlice(S), e)
	if err == nil {
		t.Fatal("expected error on Index with broken comparator")
	}
}
