// Compare allows operations on elements whose parents define a total order.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package compare

import (
	"bitbucket.org/pcasmath/object"
)

// CmpFunc should return -1 if x < y, 0 if x == y, and +1 if x > y.
type CmpFunc func(x object.Element, y object.Element) (int, error)

// Cmper is the interface satisfied by the Cmp method.
type Cmper interface {
	// Cmp returns -1 if x < y, 0 if x == y, and +1 if x > y.
	Cmp(x object.Element, y object.Element) (int, error)
}

// TotalOrder is the interface satisfied by a total order.
type TotalOrder interface {
	object.Containser
	Cmper
}
