// Search provides functions for search the entries of a slice.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package compare

import (
	"bitbucket.org/pcasmath/object"
	"bitbucket.org/pcasmath/slice"
	"sort"
)

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// Search uses the given comparison function cmp to search for x in a slice S, and return the index as specified by sort.Search. The return value is the index to insert x if x is not present (it could be S.Len()). Assumes that the slice is sorted in increasing order as determined by cmp.
func Search(cmp CmpFunc, S slice.Interface, x object.Element) (int, error) {
	// Note the length of S
	var n int
	if S != nil {
		n = S.Len()
	}
	// Is there anything to do?
	if n == 0 {
		return 0, nil
	}
	// Perform the search
	var err error
	idx := sort.Search(n, func(i int) bool {
		sgn, err2 := cmp(S.Entry(i), x)
		if err2 != nil {
			err = err2
			return false
		}
		return sgn >= 0
	})
	if err != nil {
		return 0, err
	}
	return idx, nil
}

// Index uses the given comparison function cmp to search for x in a slice S, and return the index, or -1 if not present. Assumes that the slice is sorted in increasing order as determined by cmp.
func Index(cmp CmpFunc, S slice.Interface, x object.Element) (int, error) {
	// Note the length of S
	var n int
	if S != nil {
		n = S.Len()
	}
	// Is there anything to do?
	if n == 0 {
		return -1, nil
	}
	// Perform the search
	idx, err := Search(cmp, S, x)
	if err != nil {
		return -1, err
	}
	// Check for equality
	if idx != n {
		if sgn, err := cmp(S.Entry(idx), x); err != nil {
			return -1, err
		} else if sgn == 0 {
			return idx, nil
		}
	}
	return -1, nil
}
